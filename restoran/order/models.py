# from django.db import models
# from django.utils import timezone
#
# from food.models import Table, Food
#
#
# class Order(models.Model):
#     table = models.ForeignKey(Table, on_delete=models.SET_NULL, null=True)
#     isitopen = models.BooleanField(default=True)
#     food = models.ManyToManyField(Food, related_name='orders')
#     date = models.DateTimeField(default=timezone.now)
#
#     def __str__(self):
#         return '{} + {} + {}'.format(self.table, self.isitopen, str(self.food.all().count()))
