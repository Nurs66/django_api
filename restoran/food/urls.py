from django.urls import path
from rest_framework.urlpatterns import format_suffix_patterns
from .views import (
    FoodViewList, FoodViewDetail, FoodCreateAPIView, FoodViewDelete, FoodViewUpdate,
    TableViewList, TableViewDetail,
    DepartmentViewList, DepartmentViewDetail,
    FoodCategoryViewList, FoodCategoryViewDetail
)

# API endpoints
urlpatterns = format_suffix_patterns([
    path('food/', FoodViewList.as_view(), name='food'),
    path('food/<int:id>/delete/', FoodViewDelete.as_view(), name='food_delete'),
    path('food/<int:id>/update/', FoodViewUpdate.as_view(), name='food_update'),
    path('food/<int:id>/', FoodViewDetail.as_view(), name='food_details'),
    path('food/create/', FoodCreateAPIView.as_view(), name='food_create'),
    path('table/', TableViewList.as_view(), name='table'),
    path('table/<int:pk>/', TableViewDetail.as_view(), name='table_detail'),
    path('department/', DepartmentViewList.as_view(), name='department'),
    path('department/<int:pk>/', DepartmentViewDetail.as_view(), name='department_detail'),
    path('food-category/', FoodCategoryViewList.as_view(), name='food_category'),
    path('food-category/<int:pk>/', FoodCategoryViewDetail.as_view(), name='food_category_detail')
])
